<?php
require "config.php";
require "functions.php";
if (!empty($_POST)) {
    $errors = [];
    $username = isset($_POST['username']) ? trim($_POST['username']) : '';
    $password = isset($_POST['password']) ? trim($_POST['password']) : '';
    $email = isset($_POST['email']) ? trim($_POST['email']) : '';
    if (!$username) {
        $errors[] = "Введите имя пользователя";
    } elseif (isUniqueUsername($username)) {
        $errors[] = "Такое Имя уже зарегистрированно";
    }
    if (!$password) {
        $errors[] = "Введите пароль";
    }
    if (!$email) {
        $errors[] = "Введите email";
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = "Введите корректный email!";
    } elseif (isUniqueEmail($email)) {
        $errors[] = "Такой email уже зарегистрирован";
    }
    if (empty($errors)) {
        $success = registerUser($username, $password, $email);
    }
}
$title = "Регистрация";
require "layouts/aut_header.php";
?>
    <div class="login-page">
        <div class="form">
            <form class="login-form" method="post">
                <input type="text" name="username" value="<?= isset($username) ? htmlspecialchars($username) : "" ?>"
                       placeholder="Логин"/>
                <input type="password" name="password" placeholder="Пороль"/>
                <input type="text" name="email" value="<?= isset($email) ? htmlspecialchars($email) : "" ?>"
                       placeholder="e-mail"/>
                <button name="button">Регистрация</button>
                <p></p>
                <a href="login.php">Авторизация</a>
                <br>
                <br>
                <a href="index.php">Главная</a>
            </form>
            <?php
            if (isset($success)) {
                echo "Регистрация прошла успешно!";
            }
            require "layouts/errors.php";
            ?>
        </div>
    </div>
<?php
require "layouts/aut_footer.php";
?>