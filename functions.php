<?php
function getPdo()
{
    static $pdo;
    if ($pdo === null) {
        $pdo = new PDO(PDO_DSN, PDO_USER, PDO_PASS);
    }
    return $pdo;
}

function getArticles($fetch_style = PDO::FETCH_OBJ)
{
    $pdo = getPdo();
    $query = $pdo->query('SELECT * FROM `articles` ORDER BY `id` DESC');
    return $query->fetchAll($fetch_style);
}

function registerUser($username, $password, $email)
{
    $pdo = getPdo();
    $sql = "INSERT INTO  `user` (`id` ,`user_name` ,`user_password`, `email`) VALUES (NULL ,  :user_name,  :user_password, :email);";
    $result = $pdo->prepare($sql);
    $result->bindvalue(':user_name', $username);
    $result->bindvalue(':email', $email);
    $result->bindvalue(':user_password', password_hash($password, PASSWORD_DEFAULT));
    return $result->execute();
}

function loginUser($username, $password)
{
    if (empty($username) || empty($password)) {
        return false;
    }
    $pdo = getPdo();
    $sql = "SELECT * FROM user WHERE `user_name`=:user_name";
    $sth = $pdo->prepare($sql);
    $sth->execute(['user_name' => $username]);
    $user = $sth->fetch(PDO::FETCH_ASSOC);
    if (!empty($user)) {
        if (password_verify($password, $user['user_password'])) {
            return $user;
        }
    }
    return false;
}

function isUniqueEmail($email)
{
    $sql = "SELECT count(*) FROM `user` WHERE email = :email";
    $result = getPdo()->prepare($sql);
    $result->execute(['email' => $email]);
    return (bool)$result->fetchColumn();
}

function isUniqueUsername($username)
{
    $sql = "SELECT count(*) FROM `user` WHERE user_name = :username";
    $result = getPdo()->prepare($sql);
    $result->execute(['username' => $username]);
    return (bool)$result->fetchColumn();
}

function convertDateTime($datetime, $format = "d.m.Y H:i")
{
    $timestamp = strtotime($datetime);
    return date($format, $timestamp);
}

function croppingText($text, $postfix = "...")
{
    if (strlen($text) > 250) {
        $croppedText = substr($text, 0, 250) . $postfix;
        return $croppedText;
    } else {
        return $text;
    }
}

function getArticleById($id) {
    $sql = "SELECT * FROM `articles` WHERE id = :id";
    $result = getPdo()->prepare($sql);
    $result->execute(['id' => $id]);
    return $result->fetch(PDO::FETCH_OBJ);
}

// TODO: неправильное название функции
// Функция не должна уметь так много. Что если ты её захочешь использовать еще где-то, где не нужна будет переадресация, да еще и на index.php?
// Вдруг ты сделаешь админку и там будет удаление статей? Тогда функция окажется бесполезной. Переадресациями и ответом должен заниматься контроллер,
// скрипт-обработчик, но никак не функция

function deleteArticles($id) {
    $pdo = getPdo();
    $sql = "DELETE FROM `articles` WHERE id = :id";
    $result = $pdo->prepare($sql);
    $result->execute(['id' => $id]);
    header("Location: index.php");
    exit;
}

function getMyArticles($user_name) {$pdo = getPdo();
    $sql = 'SELECT * FROM articles WHERE user_name = :user_name';
    $result = $pdo->prepare($sql);
    $result->execute(['user_name' => $user_name]);
    return $result->fetchAll(PDO::FETCH_OBJ);}