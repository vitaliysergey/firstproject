<?php
require "config.php";
require 'functions.php';
$id = isset($_GET['id']) ? (int)$_GET['id'] : 0 ;
$article = getArticleById($id);
if ($article == false) {
    header("Location: /index.php");
    exit;
}

?>
<?php
require 'layouts/header.php';
?>
<body>
<div class="single">
    <div class="container">
        <div class="col-md-8 single-main">
            <div class="single-grid">
                <img src="images/post1.jpg" alt=""/>
                <p></p>
            </div>
        </div>
        <div class="col-md-8 content-main">
            <div class="content-grid">
                <div class="content-grid-info">
                        <div class="post-info">
                            <h4>
                                <p> Номер <?= $article->id ?> </p>
                                <h5><?= "Категория: " . $article->articles_categories ?> &nbsp;</h5> <br> <h6><?= convertDateTime($article->pubdate) ?></h6> &nbsp; <h5> Автор : <?= htmlspecialchars($article->user_name) ?> </h5>
                            </h4>
                            <br>
                            <p><?= $article->text ?> </p>
                            <? if (isset($_SESSION['user_name']) && $article->user_name == $_SESSION['user_name']) { ?> <a href="" >Редактировать</a> <? } ?>
                        </div>
                    </div>
                </div>
        </div>
        <div class="categories">

            <h3>Категории</h3>
            <ul>
                <li><a href="#">О жизни</a></li>
                <li><a href="#">Программирование</a></li>
                <li><a href="#">Шутки</a></li>
            </ul>
        </div>

    </div>
</div>
<!---->
<?php
require 'layouts/footer.php';
?>
</body>
</html>


