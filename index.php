<?php
require "config.php";
require 'functions.php';
$articles = getArticles();
$title = "Главная";
require 'layouts/header.php';
?>
<div class="content">
    <div class="container">
        <div class="content-grids">
            <div class="col-md-8 content-main">
                <div class="content-grid">
                     <?php foreach ($articles as $row) { ?>
                        <div class="content-grid-info">
                            <div class="post-info">
                                <h4>
                                    <a href="single.php?id=<?=$row->id?>"> <?= $row->title ?> </a>
                                    <?= $row->articles_categories ?> &nbsp; <?= convertDateTime($row->pubdate) ?> &nbsp; <h6> <?= htmlspecialchars($row->user_name) ?> </h6>
                                </h4>
                                <br>
                                <p><?= croppingText($row->text) ?> </p>
                                <? if (isset($_SESSION['user_name']) && $row->user_name == $_SESSION['user_name']) { ?> <a href="update.php?id=<?=$row->id?>"><button>Редактировать</button></a> &nbsp; <a href="delete.php?id=<?=$row->id?>"><button>Удалить</button></a><? } ?>
                                <a href="single.php?id=<?=$row->id?>"><button>Читать дальше...</button></a>
                            </div>
                        </div>
                     <?php  }  ?>
                </div>
            </div>
            <div class="categories">
                <h3>Категории</h3>
                <ul>
                    <li><a href="#">О жизни</a></li>
                    <li><a href="#">Программирование</a></li>
                    <li><a href="#">Шутки</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php
require 'layouts/footer.php';
?>