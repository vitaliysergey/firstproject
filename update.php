<?php
require "config.php";
require "functions.php";
// присваивание id
$id = isset($_GET['id']) ? (int)$_GET['id'] : 0 ;
// подключение базы данных
$pdo = getPdo();

// TODO у тебя же есть функция getArticleById
// зачем ты здесь продублировал то же самое по сути?
// плюс почему $articleS а не $article? Разве их может быть несколько по одному id?

// выборка из базы данных по id
$sql = 'SELECT * FROM articles WHERE id = :id';
$result = $pdo->prepare($sql);
$result->execute(['id' => $id]);
$articles = $result->fetch(PDO::FETCH_ASSOC);
/////////////////////////////////////////////////////////////

// TODO А что если id некорректный и по какой-то причине не будет в базе такой статьи? Это тоже надо проверять

$articlesText = $articles['text'];
$articlesTitle = $articles['title'];
$articlesCategories = $articles['articles_categories'];
// редактирование статьи в базе данных
if (!empty($_POST)) {
    $sql = 'UPDATE articles SET text = :text, title = :title, articles_categories = :articles_categories WHERE id = :id';
    $result = $pdo->prepare($sql);

    // TODO что если в $_POST что-то не придет? Например $_POST['text'] будет отстутсовать? Неплохо бы пробелы по краям обрезать лишние

    $result->execute(['id' => $id, 'text' => $_POST['text'], 'title' => $_POST['title'], 'articles_categories' => $_POST['articles_categories']]);
    header("Location: index.php");
    exit;
}
?>
<?php
require "layouts/header.php";
?>
<div class="contact-content">
    <div class="container">
        <div class="contact-info">
            <h2>Публикация</h2>
            <p>Введите текст и тему публикации</p>
        </div>
        <div class="contact-details">
            <form method="post">
                <h3>Выберите категорию</h3>
                <br>
                <select name="articles_categories" id="articles_categories">
                    <option value="Программирование">Программирование</option>
                    <option value="Шутки">Шутки</option>
                    <option value="О жизни">О жизни</option>
                </select>
                <br>
                <br>
                <h3>Тема записи</h3>
                <br>
                <input type="text" placeholder="Тема" name="title" value="<?=htmlspecialchars($articlesTitle)?>"/>
                <h3>Тема вашей записи</h3>
                <br>
                <textarea type="text" placeholder="Сообщение" name="text"><?=htmlspecialchars($articlesText)?></textarea>
                <input type="submit" value="Отправить"/>
            </form>
        </div>
    </div>
</div>
<?php
require "layouts/footer.php";
?>

