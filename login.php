<?php
require "config.php";
require "functions.php";

if (!empty($_POST)) {
    $username = isset($_POST['username']) ? $_POST['username'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $user = loginUser($username, $password);
    $errors = [];
    if (!empty($user)) {
        $_SESSION['id'] = $user['id'];
        $_SESSION['user_name'] = $user['user_name'];
        header('Location: index.php');
        exit;
    } else {
        $errors[] = "Неверный логин или пароль!";
    }
}

?>
<?php
require "layouts/aut_header.php";
?>
<div class="login-page">
    <div class="form">
        <form class="login-form" method="post" action="login.php">
            <input type="text" name="username" placeholder="Логин"/>
            <input type="password" name="password" placeholder="Пороль"/>
            <button name="btn">Авторизация</button>
            <p></p>
            <a href="index.php">Главная</a><br><br>
            <a href="register.php">Регистрация</a>
        </form>
        <?php
        require "layouts/errors.php";
        ?>
    </div>
</div>
<?php
require "layouts/footer.php";
?>
