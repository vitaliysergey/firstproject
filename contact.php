<?php
require "config.php";
require "layouts/header.php";
?>
    <div class="contact-content">
        <div class="container">
            <div class="contact-info">
                <h2>Контакты</h2>
                <p>Задайте вопрос</p>
            </div>
            <div class="contact-details">
                <form>
                    <input type="text" placeholder="Имя" required/>
                    <input type="text" placeholder="Email" required/>
                    <textarea placeholder="Сообщение"></textarea>
                    <input type="submit" value="Отправить"/>
                </form>
            </div>
        </div>
    </div>
<?php
require "layouts/footer.php";
?>