<?php
require "config.php";
require "functions.php";
if (!empty($_POST)) {
    $title = $_POST['title'];
    $text = $_POST['text'];
    $articles_categories = $_POST['articles_categories'];
    try {
        $pdo = getPdo();
        $sql = 'INSERT INTO articles(title, text, articles_categories, user_name) VALUES(:title, :text, :articles_categories, :user_name)';
        $query = $pdo->prepare($sql);
        $result = $query->execute(['title' => $title, 'text' => $text, 'articles_categories' => $articles_categories, 'user_name' => $_SESSION['user_name']]);
        header("Location: index.php");
        exit;
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }
}
?>
<?php
require "layouts/header.php";
?>
<div class="contact-content">
    <div class="container">
        <div class="contact-info">
            <h2>Публикация</h2>
            <p>Введите текст и тему публикации</p>
        </div>
        <div class="contact-details">
            <form method="post">
                <h3>Выберите категорию</h3>
                <br>
                <select name="articles_categories" id="articles_categories">
                    <option value="Программирование">Программирование</option>
                    <option value="Шутки">Шутки</option>
                    <option value="О жизни">О жизни</option>
                </select>
                <br>
                <br>
                <h3>Тема записи</h3>
                <br>
                <input type="text" placeholder="Тема" name="title"/>
                <h3>Тема вашей записи</h3>
                <br>
                <textarea placeholder="Сообщение" name="text"></textarea>
                <input type="submit" value="Отправить"/>
            </form>
        </div>
    </div>
</div>
<?php
require "layouts/footer.php";
?>
